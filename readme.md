# Adept Wordpress Boilerplate

---
### Instructions For Use
1. [Download Wordpress](https://wordpress.org/)
2. Clone boilerplate
3. Update THEME_NAME references  
   `theme folder`  
   `style.css`  
4. Run `npm install` to install dependencies
5. Review the `__starter-files` folder for quick setup  
6. Enable included plugins  

---
### Theme Features
[Aqua Resizer](https://github.com/syamilmj/Aqua-Resizer)  
[Layout Automation For ACF Flex Content](https://gist.github.com/beaucharman/7181406)  
Task runner -- [Gulp](http://gulpjs.com/)  
___
#### Included Functions
`blog_first_image()` -- get the first image from a blog post  
`is_blog()` -- check if currently on any blog related pages  
`get_the_slug()` -- get the current page slug  
`get_the_parent_slug()` -- get the immediate parent slug of the current page  
`get_top_level_parent_slug()` -- get the highest level parent slug of the current page  