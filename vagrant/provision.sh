#!/bin/bash

# Time Zone
ln -sf /usr/share/zoneinfo/US/Eastern /etc/localtime

# restart logger
systemctl restart rsyslog

wget --no-verbose https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
wget --no-verbose http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
rpm -Uvh remi-release-7*.rpm epel-release-7*.rpm

cp -f /vagrant/etc/yum.repos.d/nginx.repo /etc/yum.repos.d/

yum -q -y install perl

# enable remi repo
# -00 processes file in paragraph mode
perl -00 -i -ne 'if (/^\[remi\]/) { $_ =~ s/enabled=0/enabled=1/g; } print $_;' /etc/yum.repos.d/remi.repo

# saftey first
echo "alias rm='rm -i'" >> /etc/bashrc
echo "alias mv='mv -i'" >> /etc/bashrc
echo "alias cp='cp -i'" >> /etc/bashrc

# better pgup/pgdn IMO
echo '"\e[5~": beginning-of-history' > .inputrc
echo '"\e[6~": end-of-history' >> .inputrc

# better mysql paging
echo "alias mysql='mysql --pager=\"less -E -X\"'" >> .bashrc

yum -q -y install man man-pages unzip git vim-enhanced php nginx php-fpm php-gd php-mysql php-pear php-mcrypt php-soap php-mbstring mariadb-server

wget --no-verbose https://wordpress.org/latest.zip
unzip -d /var/www/html/ latest.zip

ln -sf /var/www/html/wordpress /home/vagrant/wordpress

chgrp -R vagrant /var/log/php-fpm
chmod g+r /var/log/php-fpm/*
chgrp -R vagrant /var/log/nginx

#chmod 770 /home/vagrant
#gpasswd -a nginx vagrant

cp -f /vagrant/wordpress/* /var/www/html/wordpress/

chown -R vagrant:nginx /var/www/html/wordpress 2> /dev/null

systemctl enable mariadb
systemctl start mariadb

cp -f /vagrant/etc/php-fpm.d/vagrant.conf /etc/php-fpm.d/
systemctl enable php-fpm
systemctl start php-fpm

mv -f /etc/nginx/conf.d/default.conf /etc/nginx/
cp -f /vagrant/etc/nginx/conf.d/vagrant.conf /etc/nginx/conf.d/
systemctl enable nginx
systemctl start nginx
