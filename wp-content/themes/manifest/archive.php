<?php 
/*
Template Name: Archives
*/
?>

<?php get_header(); ?>


<div class= "blog-posts-archive">

	<?php 
		$i = 0; // set iterator to target first post ?>
	<?php
		if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php $i++; ?>
			<?php if ($i === 1 ) { ?>
			
			<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
		
				<div class="featured-post" style="background-image: url('<?php echo $thumb['0'];?>')">
					<div class="overlay">

						<?php get_template_part('blog-parts/blog-post-excerpt'); ?>
					</div>
				</div>
			<?php } break;?>
		<?php endwhile; endif; ?>

		 <div class="container">
		 <div class="main-content">
			<?php $i = 0; // set iterator to target first post/
			if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php $i++; 
			 if( $i !== 5 ) { ?>
					<?php get_template_part('blog-parts/blog-post-excerpt'); ?>
			<?php } ?>
			<?php if( $i === 6 )break;?>

			<?php endwhile; endif; ?>
</div>
				<?php get_template_part('blog-parts/blog-pagination'); ?>
		</div>
	
	<?php get_template_part('sidebar'); ?>


</div>


<?php get_footer(); ?>