$('.ginput_container_fileupload input[type="file"]').on('change', function(){
    var file_name = $(this).val();
    var new_file_name = file_name.replace('C:\\fakepath\\', '');
    if($(this).siblings('.file-name').length > 0){
        $(this).siblings('.file-name').remove();
    }
    $(this).parent().append('<div class="file-name"><i class="material-icons">description</i>'+new_file_name+'</div>');
});