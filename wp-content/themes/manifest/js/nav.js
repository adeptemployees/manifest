/*var n,
NAV = {
	settings: {
		bodyEl: $('body'),
		mainNav: $('.main-nav '),
		utilNav: $('.utility-nav ul'),
		navToggle: $('.menu-toggle'),
		subMenuToggleHTML: $('<span class="child-toggle"><i class="fa fa-plus-square"></i></span>')
	},
	init: function() {
		n = this.settings;

		this.bindUIActions();

		mediaCheck({
		  media: '(min-width: 800px)',
		  entry: function() {
		    NAV.desktopInit();
		  },
		  exit: function() {
		    NAV.mobileInit();
		  }
		});
	},
	bindUIActions: function() {
		n.navToggle.on('click', function() {
			n.navToggle.toggleClass('open');
			NAV.toggleMenu();
		});

		n.mainNav.find('> ul > li').on('click', '.child-toggle',function(){
			$(this).toggleClass('children-displayed');
		  	NAV.toggleSubMenu($(this));
		});
	},
	toggleMenu: function() {
		n.bodyEl.toggleClass('menu-displayed');
	},
	toggleSubMenu: function(element) {
		element.parent().toggleClass('children-displayed');
	},
	mobileInit: function() {
		n.bodyEl.removeClass('desktop-menu').addClass('mobile-menu');
		n.utilNav.clone().addClass('utility-nav-duplicate').appendTo(n.mainNav);
		n.mainNav.find('.menu-item-has-children').prepend(n.subMenuToggleHTML);
	},
	desktopInit: function() {
		n.bodyEl.removeClass('mobile-menu').addClass('desktop-menu');
		n.mainNav.find('.child-toggle').remove();
		$('.utility-nav-duplicate').remove();
	}
};

$(document).ready(function() {
	NAV.init();
});*/

var nav = priorityNav.init({
    mainNavWrapper: ".main-nav", // mainnav wrapper selector (must be direct parent from mainNav)
    mainNav: ".primary", // mainnav selector. (must be inline-block)
    navDropdownLabel: 'menu',
    navDropdownClassName: "nav__dropdown", // class used for the dropdown.
    navDropdownToggleClassName: "nav__dropdown-toggle", // class used for the dropdown toggle.
    navDropdownBreakpointLabel: "menu",
    breakPoint: 320,
});

$(window).scroll(function() {
	var header = $('.site-header');
	if($(window).scrollTop() > (header.height() * 2)){
		header.css('position', 'fixed');
	} else {
		header.css('position', 'relative');
	}
});