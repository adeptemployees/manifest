$('.footer-social-nav a').on('click', function(e){

    e.preventDefault();
    var destination = $(this).attr('href');

    if(destination.indexOf('facebook') != -1) {
        ga('send', 'event', 'button', 'click', 'Facebook');
    }

    if(destination.indexOf('twitter') != -1) {
        ga('send', 'event', 'button', 'click', 'Twitter');
    }

    if(destination.indexOf('linkedin') != -1) {
        ga('send', 'event', 'button', 'click', 'LinkedIn');
    }

    if(destination.indexOf('google') != -1) {
        ga('send', 'event', 'button', 'click', 'Google+');
    }

    window.location = destination;

});