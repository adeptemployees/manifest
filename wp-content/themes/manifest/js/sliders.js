var dots = false;
var vertical = false;
var slidesToShow = 1;

var prevBtn = '<button type="button" class="slick-prev"><i class="material-icons">keyboard_arrow_left</i></button>';
var nextBtn = '<button type="button" class="slick-next"><i class="material-icons">keyboard_arrow_right</i></button>';

if(params.vertical) {
    vertical = true;
    if(params.content_type == 'simple') {
        slidesToShow = 6;
    }
    prevBtn = '<button type="button" class="slick-prev"><i class="material-icons">keyboard_arrow_up</i></button>';
    nextBtn = '<button type="button" class="slick-next"><i class="material-icons">keyboard_arrow_down</i></button>';
}

if(params.simple) {
    dots = true;
}

if(params.vertical) {
    $(params.primary_selector).slick({
        autoplay : true,
        mobileFirst : true,
        arrows : true,
        slidesToShow : slidesToShow,
        vertical: true,
        prevArrow : prevBtn,
        nextArrow : nextBtn,
        autoPlay: true
    });
} else {
    $(params.primary_selector).slick({
        arrows : false,
        dots: true,
        slidesToShow : slidesToShow,
        prevArrow : prevBtn,
        nextArrow : nextBtn,
        responsive : [{
            breakpoint : 767,
            settings : {
                adaptiveHeight: true,
                arrows : true,
                dots : false,
            }
        }]
    });
}