<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie8"> <![endif]-->
<!--[if gt IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]> <!--> <html class="no-js"> <!--><![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php the_title(); ?></title>

    <?php wp_head(); ?>

    <script type='text/javascript'>
        (function (d, t) {
            var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
            bh.type = 'text/javascript';
            bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=ey9wwgyoqljdti1mjy2wyq';
            s.parentNode.insertBefore(bh, s);
        })(document, 'script');
    </script>
</head>

<body <?php body_class(); ?>>

    <!--[if lt IE 9]>
        <div class="browser-notice">
            <p>You are using an outdated browser. Please <a href="http://browsehappy.com/" target="_blank">update your browser</a> to improve your experience.  </p>
        </div>
    <![endif]-->


    <div class="site-wrapper">
        <header class="site-header group">

                <a href="<?php bloginfo('url') ?>" class="logo"><img src="<?php bloginfo('template_url'); ?>/img/logo.png" alt=""></a>

                <nav class="main-nav group">
                    <?php
                    $walker = new Menu_With_Description;
                    $args = array(
                        'theme_location' => 'main_nav',
                        'menu' => 'main_nav',
                        'container' => '',
                        'menu_class' => 'primary',
                        'echo' => true,
                        'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
                        'depth' => 0,
                        'walker' => $walker
                        );
                    wp_nav_menu( $args ); 
                    ?>
                </nav>

        </header>

        <div class="content-wrapper">