<?php 
//default page layout
get_header(); 

	if ( have_posts() ) : while ( have_posts() ) : the_post();  

		if(have_rows('page_content')) :
			
			$block_count = 1;
			$new_bg_count = 0;

			while(have_rows('page_content')) : the_row();
				
				if( get_row_layout() === 'start_new_background' || get_row_layout() === 'billboard') {
					//close current background block
					//unless it's the first new_background
					if($new_bg_count > 0) {
						echo '</div>';
					}
					$new_bg_count++;
				}

				ACF_Layout::render(get_row_layout(), $block_count);

				$block_count++;
			endwhile;
			
			echo '</div>'; //close new-background section
		endif;

	endwhile; endif;  

get_footer(); 
?>
