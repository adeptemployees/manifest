<div class="block--<?php echo $block_count; ?> slider slider-list">
    <?php
        wp_enqueue_script( 'slick' );
    ?>
    <div class="container">
        <?php if(get_sub_field('has_content_block') && get_sub_field('static_content_position') == 'before'): ?>
            <article>
                <?php echo get_sub_field('static_content'); ?>
            </article>
        <?php endif; ?>

        <ul class="primary-slider primary-slider--<?php echo get_sub_field('item_type'); ?>-items <?php echo(get_sub_field('orientation') == 'vertical' ? 'primary-slider--vertical' : 'primary-slider--horizontal'); ?>">
            <?php if(get_sub_field('item_type') == 'complex'): ?>
                <?php if(have_rows('complex_list_items')): ?>
                    <?php while(have_rows('complex_list_items')): the_row(); ?>
                        <li><?php echo get_sub_field('complex_item'); ?></li>
                    <?php endwhile; ?>
                <?php endif; ?>
            <?php else: ?>
                <?php if(have_rows('simple_list_items')): ?>
                    <?php while(have_rows('simple_list_items')): the_row(); ?>
                        <li><?php echo get_sub_field('simple_item'); ?></li>
                    <?php endwhile; ?>
                <?php endif; ?>
            <?php endif; ?>
        </ul>

        <?php if(get_sub_field('has_content_block') && get_sub_field('static_content_position') == 'after'): ?>
            <article>
                <?php echo get_sub_field('static_content'); ?>
            </article>
        <?php endif; ?>
    </div>

</div>

<?php
    $vertical = false;
    if(get_sub_field('orientation') == 'vertical') {
        $vertical = true;
    }
    $params = [
        'primary_selector' => '.block--'.$block_count.' .primary-slider',
        'vertical' => $vertical,
        'content_type' => get_sub_field('item_type')
    ];
    wp_localize_script( 'sliders', 'params', $params );
    wp_enqueue_script( 'sliders' );
?>