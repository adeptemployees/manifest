<?php foreach(get_sub_field('opportunities') as $opportunity): ?>
    <a href="<?php the_permalink($opportunity); ?>" class="liner">
        <h3><?php echo get_the_title($opportunity); ?></h3>
        <div class="liner__details">
            <?php foreach(get_the_terms( $opportunity, 'opportunity-types' ) as $term): ?>
                <span class="opportunity__details-type"><?php echo $term->name; ?></span>
            <?php endforeach; ?>
            <span class="opportunity__details-location"><?php echo the_field('opportunity_city', $opportunity->ID); ?>, <?php echo the_field('opportunity_state', $opportunity->ID); ?></span>
        </div>
        <span class="liner__link">Get Details <i class="material-icons reverse">keyboard_backspace</i></span>
    </a>
<?php endforeach; ?>