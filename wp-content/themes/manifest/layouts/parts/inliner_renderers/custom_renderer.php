<?php foreach(get_sub_field('blocks') as $block): ?>
    <?php if($block['has_link']): ?>
        <a href="<?php echo($block['link_type'] = 'internal' ? $block['internal_url'] : $block['custom_url']); ?>" class="liner">
    <?php else: ?>
        <span class="liner">
    <?php endif; ?>
        <h3><?php echo $block['title']; ?></h3>
        <div class="liner__details">
            <?php echo $block['description']; ?>
        </div>
        <span class="liner__link"><?php echo $block['link_text']; ?></span>
    <?php if($block['has_link']): ?>
        </a>
    <?php else: ?>
        </span>
    <?php endif; ?>
<?php endforeach; ?>