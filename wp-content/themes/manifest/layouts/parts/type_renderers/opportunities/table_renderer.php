<?php
    // Table display for Opprtunities
    $term_args = array(
        'taxonomy' => 'opportunity-types',
        'hide_empty' => true
    );
    $terms = get_terms( $term_args );

    $skill_args = array(
        'taxonomy' => 'opportunity-skills',
        'hide_empty' => true
    );
    $skills = get_terms( $skill_args );

    $active_locations_args = array(
        'post_type' => 'opportunity',
        'posts_per_page' => -1
    );
    $active_locations_query = new WP_Query( $active_locations_args );
    $active_locations = array();
    if ( $active_locations_query->have_posts() ) {
        while ( $active_locations_query->have_posts() ) {
            $active_locations_query->the_post();
            $active_locations[get_field('opportunity_city').'--'.get_field('opportunity_state')] = get_field('opportunity_city').', '.get_field('opportunity_state');
        }
    }

    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

    $search_type = $_GET['opportunity_type'];
    $search_location = $_GET['opportunity_loc'];
    $search_skill = $_GET['skill_type'];

    if((!is_null($search_type) && $search_type !== '') && (!is_null($search_skill) && $search_skill !== '')) {
        $tax_search = array();
        $tax_search['relation'] = 'AND';
    } elseif ((!is_null($search_type) && $search_type !== '') || (!is_null($search_skill) && $search_skill !== '')) {
        $tax_search = array();
    } else {
        $tax_search = null;
    }

    if(!is_null($search_type) && $search_type !== '') {
        $tax_search[] = array(
            array(
                'taxonomy' => 'opportunity-types',
                'field' => 'slug',
                'terms' => $search_type,
            )
        );
    }

    if(!is_null($search_skill) && $search_skill !== '') {
        $tax_search[] = array(
            array(
                'taxonomy' => 'opportunity-skills',
                'field' => 'slug',
                'terms' => $search_skill,
            )
        );
    }

    $meta_query = null;
    if(!is_null($search_location) && $search_location !== '') {
        $location_elements = explode('--', $search_location);
        $city = str_replace('+', ' ', $location_elements[0]);
        $state = $location_elements[1];
        $meta_query = array(
            array(
                'key' => 'opportunity_city',
                'value' => $city,
                'type' => 'TEXT',
                'compare' => '='
            ),
            array(
                'key' => 'opportunity_state',
                'value' => $state,
                'type' => 'TEXT',
                'compare' => '='
            )
        );
    }

    if(get_sub_field('posts_per_page')) {
        $posts_per_page = get_sub_field('posts_per_page');
    } else {
        $posts_per_page = get_option( 'posts_per_page' );
    }

    $args = array(
        'post_type' => 'opportunity',
        'posts_per_page' => $posts_per_page,
        'tax_query' => $tax_search,
        'meta_query' => $meta_query,
        'orderby' => 'meta_value',
        'meta_type' => 'CHAR',
        'meta_key' => 'opportunity_city',
        'order' => 'ASC',
        'paged' => $paged
    );
    $query = new WP_Query( $args );
?>

<section class="opp">

    <form role="search" action="<?php echo site_url('/opportunities/all-opportunities'); ?>" method="get" id="opportunitysearchform">
        <select name="opportunity_loc" onchange="this.form.submit()">
            <option value="">Filter By Location</option>
            <?php foreach ($active_locations as $key => $value) : ?>
                <option value="<?php echo $key; ?>" <?php echo ($search_location == $key ? 'selected="selected"' : '' ); ?>><?php echo $value; ?></option>
            <?php endforeach; ?>
        </select>

        <select name="opportunity_type" onchange="this.form.submit()">
            <option value="">Filter By Type</option>
            <?php foreach($terms as $term): ?>
                <option value="<?php echo $term->slug; ?>" <?php echo ($search_type == $term->slug ? 'selected="selected"' : '' ); ?>><?php echo $term->name; ?></option>
            <?php endforeach; ?>
        </select>

        <select name="skill_type" onchange="this.form.submit()">
            <option value="">Filter By Skill</option>
            <?php foreach($skills as $skill): ?>
                <option value="<?php echo $skill->slug; ?>" <?php echo ($search_skill == $skill->slug ? 'selected="selected"' : '' ); ?>><?php echo $skill->name; ?></option>
            <?php endforeach; ?>
        </select>
  </form>

    <div class="opp-table">
        <?php
            if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();

                get_template_part('layouts/parts/type_renderers/opportunities/opportunity-table-excerpt');

            endwhile;

            else:
                echo '<div class="empty-search">';
                if(is_null($search_type) && is_null($search_location)) {
                    if(get_field('empty_open_opportunities', 'options')):
                        echo '<h2>'.get_field('empty_open_opportunities', 'options').'</h2>';
                    else:
                        echo '<h2>There are currently no opportunities available.</h2>';
                    endif;
                } else {
                    if(get_field('empty_open_opportunities_searched', 'options')):
                        echo '<h2>'.get_field('empty_open_opportunities_searched', 'options').'</h2>';
                    else:
                        echo '<h2>There are currently no opportunities open that meet your search criteria.</h2>';
                    endif;
                }
                echo '</div>';

            endif;
        ?>
    </div>
    
</section>