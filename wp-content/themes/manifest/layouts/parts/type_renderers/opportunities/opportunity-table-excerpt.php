<article>
    <div class="opp-title">
        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    </div>
    <div class="opp-loc">
        <?php echo get_field('opportunity_city'); ?>, <?php echo get_field('opportunity_state'); ?>
    </div>
    <div class="opp-type">
        <?php
            $posttags = $terms = wp_get_post_terms( $post->ID, 'opportunity-types' );
            foreach($posttags as $tag) { $keywords[] = $tag->name; }
            $page_keywords = implode(',',$keywords);
            echo $page_keywords;
        ?>
    </div>
    <div class="opp-action">
        <a href="<?php the_permalink(); ?>">Get Details <i class="material-icons reverse">keyboard_backspace</i></a>
    </div>
</article>