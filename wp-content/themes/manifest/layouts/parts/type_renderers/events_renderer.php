<?php
    
    $months = array(
        1 => 'January',
        2 => 'February',
        3 => 'March',
        4 => 'April',
        5 => 'May',
        6 => 'June',
        7 => 'July',
        8 => 'August',
        9 => 'September',
        10 => 'October',
        11 => 'November',
        12 => 'December'
    );

    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

    $search = $_GET['event_type'];
    $tax_search = null;
    if(!is_null($search) && $search !== '') {
        $tax_search = array(
            array(
                'taxonomy' => 'event-types',
                'field' => 'slug',
                'terms' => $search,
            )
        );
    }
    $search_month = (int)$_GET['event_month'];
    $s_start_date = $months[$search_month].' '.date('Y');
    $s_end_date = $months[$search_month + 1].' '.date('Y');

    if(get_sub_field('posts_per_page')) {
        $posts_per_page = get_sub_field('posts_per_page');
    } else {
        $posts_per_page = get_option( 'posts_per_page' );
    }

    $term_args = array(
        'taxonomy' => 'event-types',
        'hide_empty' => false
    );
    $terms = get_terms( $term_args );

    if(!is_null($search_month) && $search_month !== 0) {
        $meta_query = array(
            array(
                'key' => 'start_date',
                'value' => date('Ymd', strtotime($s_start_date)),
                'type' => 'DATE',
                'compare' => '>='
            ),
            array(
                'key' => 'start_date',
                'value' => date('Ymd', strtotime($s_end_date)),
                'type' => 'DATE',
                'compare' => '<'
            )
        );
    } else {
        $meta_query = array(
            array(
                'key' => 'start_date',
                'value' => date('Ymd'),
                'type' => 'DATE',
                'compare' => '>='
            )
        );
    }

    $args = array(
        'post_type' => 'event',
        'posts_per_page' => $posts_per_page,
        'orderby' => 'meta_value_num',
        'order' => 'ASC',
        'meta_key' => 'start_date',
        'tax_query' => $tax_search,
        'meta_query' => $meta_query,
        'paged' => $paged
    );
    $query = new WP_Query( $args );

    $display_month = null;
    $number_of_posts = ($query->have_posts()) ? sizeof($query->posts) : 0;
?>

<?php if($number_of_posts > 5): ?>
    <div class="event-posts-wrapper">

        <form role="search" action="<?php echo site_url('/events'); ?>" method="get" id="eventsearchform">
            <select name="event_type" onchange="this.form.submit()">
                <option value="">Filter By Event Type</option>
                <?php foreach($terms as $term): ?>
                    <option value="<?php echo $term->slug; ?>" <?php echo ($search == $term->slug ? 'selected="selected"' : '' ); ?>><?php echo $term->name; ?></option>
                <?php endforeach; ?>
            </select>
            <select name="event_month" onchange="this.form.submit()">
                <option value="">Filter By Event Month</option>
                <?php foreach($months as $key => $value): ?>
                    <option value="<?php echo $key; ?>" <?php echo ($search_month == $key ? 'selected="selected"' : '' ); ?>><?php echo $value; ?></option>
                <?php endforeach; ?>
            </select>
      </form>
     </div>
 <?php else: ?>
    <div class="no-posts-spacer"></div>
 <?php endif; ?>

    <?php
        if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); 

            if(!isset($search)){
                $event_month = date('n', strtotime(get_field('start_date')));
                if(is_null($display_month)) {
                    $display_month = $event_month;
                    echo '<div class="month-group">Upcoming '.date('F', strtotime(get_field('start_date'))).' Events</div>';
                } elseif ($display_month != $event_month) {
                    $display_month = $event_month;
                    echo '<div class="month-group">'.date('F', strtotime(get_field('start_date'))).' Events</div>';
                }
            }

            get_template_part('blog-parts/event-post-excerpt');

        endwhile;

        else:

            echo '<div class="empty-search">';
                if(get_field('empty_searched_events', 'options')):
                    echo '<h2>'.get_field('empty_searched_events', 'options').'</h2>';
                else:
                    echo '<h2>There are currently no events scheduled that meet your search criteria.</h2>';
                endif;
            echo '</div>';

        endif; 

    ?>
    <?php if(get_sub_field('paginate')): ?>
        <nav class="pagintation--posts">
            <?php
                if ( $query->max_num_pages > 1 ) {
                    $big = 99999999;
                    echo paginate_links(array(
                        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                        'format' => '/page/%#%',
                        'total' => $query->max_num_pages,
                        'current' => max(1, get_query_var('paged')),
                        'show_all' => false,
                        'end_size' => 2,
                        'mid_size' => 3,
                        'prev_next' => true,
                        'prev_text' => 'prev',
                        'next_text' => 'next',
                        'type' => 'list'
                    ));
                }
            ?>
        </nav>
    <?php endif; ?>
</div>