<article>
    <?php echo get_sub_field('featured_content'); ?>
</article>

<style type="text/css">
    .block--<?php echo $block_count; ?> aside li {
        color: <?php echo get_sub_field('item_text_color'); ?>;
    }
    .block--<?php echo $block_count; ?> aside li:before {
        color: <?php echo get_sub_field('list_item_icon_color'); ?>;
    }
</style>

<aside class="featured-list-renderer">
    <ul class="icon-<?php echo get_sub_field('list_item_icon'); ?>">
        <?php while(has_sub_field('featured_items')) : ?>
            <li><?php echo the_sub_field('item'); ?></li>
        <?php endwhile; ?>
    </ul>
</aside>