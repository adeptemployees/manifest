<?php
    $trigger = false;
?>

    <ul class="primary-slider">
        <?php while(has_sub_field('scrolling_list_items')) : ?>
            <li><?php echo the_sub_field('item'); ?></li>
        <?php endwhile; ?>
    </ul>

<?php if(get_sub_field('has_trigger_list')) : ?>
        <?php $trigger = '.block--'.$block_count.' .trigger-list'; ?>
        <ul  class="trigger-list">
            <?php while(has_sub_field('trigger_items')) : ?>
                <li><?php echo the_sub_field('trigger_item'); ?></li>
            <?php endwhile; ?>
        </ul>
<?php endif; ?>

<?php
    $params = [
        'primary_selector' => '.block--'.$block_count.' .primary-slider',
        'trigger_selector' => $trigger,
        'vertical' => get_sub_field('has_vertical_trigger_list')
    ];
    wp_localize_script( 'sliders', 'params', $params );
    wp_enqueue_script( 'sliders' );