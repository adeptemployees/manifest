<div class="side-content">
    <?php echo get_sub_field('side_content'); ?>
</div>
<div class="selected-content selected-content--<?php echo get_sub_field('post_type'); ?>">
    <?php switch (get_sub_field('post_type')) {
        case 'event':
            include 'partials/event_renderer.php';
            break;
        
        case 'opportunity':
            echo 'Hi';
            break;

        default:
            echo 'Ho';
            break;
    }
    ?>
</div>