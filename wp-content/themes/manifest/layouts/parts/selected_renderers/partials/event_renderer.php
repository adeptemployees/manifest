<?php if(sizeof(get_sub_field('events')) > 0): ?>
    <?php foreach (get_sub_field('events') as $event) : ?>
        <div class="event-post--excerpt--simple">
            <div class="excerpt-image">
                <?php
                    //get featured image or first image in post
                    if ( has_post_thumbnail($event->ID) ) {  
                        $img = wp_get_attachment_url(get_post_thumbnail_id($event->ID));
                    } else {
                        $img = false;
                    }
                    
                    if($img && $img !== "") {
                        echo '<a href="'.get_permalink($event->ID).'"><img src="'.$img.'"></a>';
                    }
                ?>
            </div>

            <div class="excerpt-content">
                <ul class="event-details">
                    <li>
                        <i class="material-icons">event</i>
                        <?php echo date('F j', strtotime(get_field('start_date', $event->ID))); ?>
                        <?php if(get_field('start_date', $event->ID) != get_field('end_date', $event->ID)): ?>
                            <?php if($start_month == $end_month): ?>
                                - <?php echo $end_day; ?>
                            <?php else: ?>
                                <?php if(get_field('end_date', $event->ID) != ''): ?>
                                    - <?php echo $end_month; ?> <?php echo $end_day; ?>
                                <?php endif; ?>
                            <?php endif; ?>  
                        <?php endif; ?>        
                    </li>
                    <li><i class="material-icons">room</i> <?php echo get_field('location_display_name', $event->ID); ?></li>
                </ul>
                <h3 class="event-post--excerpt-title"><a href="<?php echo get_permalink($event->ID); ?>"><?php echo get_the_title($event->ID); ?></a></h3>

                <?php
                    echo pd_get_excerpt_from_content($event->post_content, 200);
                ?>

            </div>
        </div>
    <?php endforeach; ?>
<?php else: ?>
    <div class="empty-set">There are currently no featured events scheduled</div>
<?php endif; ?>