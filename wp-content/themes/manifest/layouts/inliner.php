<div class="block--<?php echo $block_count; ?> inliner">
    <?php
        switch (get_sub_field('item_creation')) {
            case 'create':
                include 'parts/inliner_renderers/custom_renderer.php';
                break;

            case 'post_select':
                switch (get_sub_field('post_blocks')) {
                    case 'pages':
                        # code...
                        break;

                    case 'events':
                        # code...
                        break;

                    case 'opportunities':
                        include 'parts/inliner_renderers/opportunity_renderer.php';
                        break;
                    
                    default:
                        # code...
                        break;
                }
                //include 'parts/selected_renderers/side_content_renderer.php';
                break;
            
            default:
                echo 'No layout template defined.';
                break;
        }
    ?>
</div>