<div class="block--<?php echo $block_count; ?> simple-side-by-side <?php echo (get_sub_field('card_border_color') != '' ? 'has-borders' : ''); ?>">
    <div class="container">
        
    <?php if(get_sub_field('card_border_color') != ''): ?>
        <style type="text/css">
            .block--<?php echo $block_count; ?> .card {
                border: 1px solid <?php echo get_sub_field('card_border_color'); ?>;
            }
        </style>
    <?php endif; ?>

        <?php the_sub_field('intro_content'); ?>
        
        <?php 
            $column_count = get_sub_field('column_count');
            $card_count = count(get_sub_field('cards'));
            $odd_card_count = ($card_count % $column_count !== 0 ? 'odd-card-count' : '');
            
            echo '<div class="card-wrapper cols-'.$column_count.' '.$odd_card_count.'">';

            while(has_sub_field('cards')) :
                //get image
                $img_id = get_sub_field('image');
                if($img_id) {
                    $image = wp_get_attachment_image_src($img_id, 'thumb');
                    $image_alt = get_post_meta($img_id, '_wp_attachment_image_alt', true);
                }

                //card description
                $card_description = get_sub_field('card_description');

        ?>
                <div class="card">
                    <?php if($img_id) {echo '<div class="card-image"><img src="'.$image[0].'" width="'.$image[1].'" height="'.$image[2].'" alt="'.$image_alt.'"></div>';} ?>
                    <div class="card-text">
                        <?php echo $card_description; ?>
                    </div> <!-- card-text -->
                </div> <!-- card -->

        <?php 
            endwhile;

            echo '</div> <!-- card-wrapper -->';
        ?>
    
        <?php the_field('outro_content'); ?>

    </div> <!-- container -->
</div> <!-- block -->