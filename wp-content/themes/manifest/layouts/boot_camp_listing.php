<div class="block--<?php echo $block_count; ?> posts-archive bootcamp-archive">
<?php
    $args = array(
        'post_type' => 'event',
        'order' => 'ASC',
        'orderby' => 'meta_value_date',
        'tax_query' => array(
            array(
                'taxonomy' => 'event-types',
                'field' => 'slug',
                'terms' => 'boot-camps',
            ),
        ),
        'meta_query' => array(
            array(
                'key' => 'start_date',
                'value' => date('Ymd'),
                'type' => 'DATE',
                'compare' => '>='
            )
        ),
    );

    $query = new WP_Query( $args );
?>
    <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
        <?php get_template_part('blog-parts/event-post-excerpt'); ?>
    <?php endwhile; ?>
    <?php else: ?>
        <div class="empty-search">
            <?php if(get_field('empty_bootcamps', 'options')): ?>
                <h2><?php echo get_field('empty_bootcamps', 'options'); ?></h2>
            <?php else: ?>
                <h2>There are currently no events scheduled that meet your search criteria.</h2>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</div>