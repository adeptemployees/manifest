<div class="block--<?php echo $block_count; ?> special-list <?php echo get_sub_field('list_type'); ?>">

    <div class="container">
        <?php
            switch (get_sub_field('list_type')) {
                case 'featured_content':
                    include('parts/featured_content_list_renderer.php');
                    break;

                case 'scroller':
                    wp_enqueue_script( 'slick' );
                    include('parts/slider_list_renderer.php');
                    break;
            }
        ?>
    </div>

</div>