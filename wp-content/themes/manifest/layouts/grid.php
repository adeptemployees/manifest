<style type="text/css">
    .block--<?php echo $block_count; ?> {
        margin-bottom: <?php echo get_sub_field('grid_spacing'); ?>em;
        margin-top: <?php echo get_sub_field('grid_spacing'); ?>em;
    }
    .block--<?php echo $block_count; ?> .container {
        justify-content: <?php echo get_sub_field('grid_origin'); ?>;
        align-items: <?php echo get_sub_field('vertical_positioning'); ?>;
        <?php if(get_sub_field('maximum_width') > 0): ?>
            max-width: <?php echo get_sub_field('maximum_width'); ?>px;
            margin-left: auto;
            margin-right: auto;
        <?php endif; ?>
    }
</style>

<div class="block--<?php echo $block_count; ?> grid grid-cols-<?php echo get_sub_field('columns'); ?>">

    <div class="container">
        
        <?php while(has_sub_field('items')) : ?>

            <div class="grid-item" style="padding:<?php the_sub_field('padding_top'); ?>em <?php the_sub_field('padding_right'); ?>em <?php the_sub_field('padding_bottom'); ?>em <?php the_sub_field('padding_left'); ?>em;">

                <?php the_sub_field('content'); ?>

            </div>

        <?php endwhile; ?>

    </div>

</div>