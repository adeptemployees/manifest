<div class="block--<?php echo $block_count; ?> billboard">

<?php 
	//background_photo
	if(get_sub_field('background_photo')) {

		$photo_url = get_sub_field('background_photo');
		$photo_url = aq_resize($photo_url, 1500, null, true, true, true);

		echo '
			<style>
				.block--'.$block_count.' .billboard--image {
					background-image: url('.$photo_url.');
					background-repeat: no-repeat;
					background-position: '.get_sub_field('background_x_position').'% '.get_sub_field('background_y_position').'%;
					background-size: cover;
				}
				@media screen and (min-width: 1500px) {
					.block--'.$block_count.' .billboard--image {
						background-size: 100% auto;
					}
				}
			</style>';

		echo '<div class="billboard--image"></div>';
	}

	//background_color
	if(get_sub_field('background_color')) {
		$background_color_hex = get_sub_field('background_color');
		$background_color_opacity = get_sub_field('background_color_opacity');
		$background_color_opacity = (int)$background_color_opacity / 100;
		$background_color = hex2rgba($background_color_hex, $background_color_opacity);
		
		echo '<div class="billboard--bg-color" style="background-color:'.$background_color.';"></div>';
	}

	//inner glow
	if(get_sub_field('inner_glow')) {
		$glow_color = get_sub_field('glow_color');
		$glow_opacity = get_sub_field('glow_opacity');
		$glow_opacity = (int)$glow_opacity / 100;
		$glow_color = hex2rgba($glow_color, $glow_opacity);
		$glow_size = get_sub_field('glow_size');
		$inner_glow = 'inset 0 0 '.$glow_size.'px '.$glow_color;
		echo '
			<style>
				.block--'.$block_count.' .billboard--bg-color {
					-webkit-box-shadow: '.$inner_glow.';
					-moz-box-shadow: '.$inner_glow.';
					-o-box-shadow: '.$inner_glow.';
					box-shadow: '.$inner_glow.';
				}
			</style>';
	}
	else {
		$inner_glow = '';
	}

	//gradient
	if(get_sub_field('background_gradient')){
		$gradient_color = get_sub_field('gradient_color');
		$gradient_color_transparent = hex2rgba($gradient_color, .001);
		$gradient_opacity = get_sub_field('gradient_opacity');
		$gradient_opacity = (int)$gradient_opacity / 100;
		$gradient_color = hex2rgba($gradient_color, $gradient_opacity);

		$gradient_start_position = get_sub_field('gradient_start_position');
		if($gradient_start_position === 'top') {
			$bg_gradient = 'linear-gradient('.$gradient_color.', '.$gradient_color_transparent.')';
		}
		else { //$gradient_start_position === 'bottom'
			$bg_gradient = 'linear-gradient(bottom, '.$gradient_color.', '.$gradient_color_transparent.')';
		}
		echo '
			<style>
				.block--'.$block_count.' .billboard--bg-color {
					background: -webkit-'.$bg_gradient.';
					background: -o-'.$bg_gradient.';
					background: -moz-'.$bg_gradient.';
					background: '.$bg_gradient.';
				}
			</style>';
	}
	else {
		$bg_gradient = '';
	}

?>

	<div class="container">
		<?php
			$content_max_width = get_sub_field('content_max_width');
			$content_max_width = 'max-width: '.$content_max_width .'px;';

			$content_top_padding = get_sub_field('top_padding');
			$content_top_padding = 'padding-top: '.$content_top_padding.'em;';

			$content_bottom_padding = get_sub_field('bottom_padding');
			$content_bottom_padding = 'padding-bottom: '.$content_bottom_padding.'em;';

			$content_alignment = get_sub_field('content_alignment');
			
		?>
		<div class="billboard--content <?php echo 'align-content-'.$content_alignment; ?>" style="<?php echo $content_max_width.''.$content_top_padding.''.$content_bottom_padding; ?>">
			<?php the_sub_field('content'); ?>
		</div> <!-- billboard content -->

		<?php if(get_sub_field('scroll_enticer')) {echo '<span class="scroll-off-screen"><i class="material-icons">&#xE5DB;</i>'.get_sub_field('scroll_enticer').'</span>';} ?>
	</div> <!-- container -->
	
</div> <!-- billboard -->

