<div class="block--<?php echo $block_count; ?> selected-posts">
    <div class="container">
    <?php
        switch (get_sub_field('layout')) {
            case 'side_content':
                include 'parts/selected_renderers/side_content_renderer.php';
                break;
            
            default:
                echo 'No layout template defined.';
                break;
        }
    ?>
    </div>
</div>