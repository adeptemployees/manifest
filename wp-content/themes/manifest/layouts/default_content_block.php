<?php 
	$columns = get_sub_field('columns');
	$column_count = count($columns);

	$block_classes = '';
	if($columns[0]['content_type'] === 'photo') {
		$block_classes .= ' has-photo photo-first';
	}
	elseif($columns[1]['content_type'] === 'photo') {
		$block_classes .= ' has-photo photo-last';
	}
	else {
		$block_classes .= ' container';
	}

	echo '<div class="block--'.$block_count.' default-content-block--'.$column_count.'column '.$block_classes.'">';

	$current_column = 1;


	while(has_sub_field('columns')) :
		//get content type for current column
		$content_type = get_sub_field('content_type');

		echo '<div class="column-'.$current_column.' '.$content_type.'">';
		if($content_type === 'text') :
			echo '<div class="text-wrapper default">'.get_sub_field('content').'</div>';
		endif; //end $content_type == 'text';


		if($content_type === 'photo') :
			//photo
			$photo_id = get_sub_field('photo');
			$photo = wp_get_attachment_image_src($photo_id, 'full');
			$photo_url = aq_resize($photo[0], 800, null, true, true, true);

			//color overlay
			if(get_sub_field('color_overlay')) {
				$color_overlay = get_sub_field('overlay_color');
				$color_overlay_opacity = get_sub_field('overlay_color_opacity');
				$color_overlay_opacity = (int)$color_overlay_opacity / 100;
				$color_overlay = hex2rgba($color_overlay, $color_overlay_opacity);
				$overlay = '<div class="overlay" style="background-color: '.$color_overlay.';"></div>';
			}
			else {
				$overlay = '';
			}

			//bg gradient
			if(get_sub_field('background_gradient')){
				$gradient_color = get_sub_field('gradient_color');
				$gradient_color_transparent = hex2rgba($gradient_color, .001);
				$gradient_opacity = get_sub_field('gradient_opacity');
				$gradient_opacity = (int)$gradient_opacity / 100;
				$gradient_color = hex2rgba($gradient_color, $gradient_opacity);

				$gradient_start_position = get_sub_field('gradient_start_position');
				if($gradient_start_position === 'top') {
					$bg_gradient = 'linear-gradient('.$gradient_color.', '.$gradient_color_transparent.')';
				}
				else { //$gradient_start_position === 'bottom'
					$bg_gradient = 'linear-gradient(bottom, '.$gradient_color.', '.$gradient_color_transparent.')';
				}

				$gradient = '<div class="gradient" style="-webkit-background-image:'.$bg_gradient.'; -moz-background-image:'.$bg_gradient.'; -o-background-image:'.$bg_gradient.'; background-image:'.$bg_gradient.';"></div>';
			}
			else {
				$gradient = '';
			}
			
			//inner glow
			if(get_sub_field('inner_glow')){
				$glow_color = get_sub_field('glow_color');
				$glow_opacity = get_sub_field('glow_opacity');
				$glow_opacity = (int)$glow_opacity / 100;+
				$glow_color = hex2rgba($glow_color, $glow_opacity);
				$glow_size = get_sub_field('glow_size');

				$inner_glow = 'inset 0 0 '.$glow_size.'px '.$glow_color.';';

				$glow = '<div class="glow" style="-webkit-box-shadow: '.$inner_glow.'; -moz-box-shadow: '.$inner_glow.'; -o-box-shadow: '.$inner_glow.'; box-shadow: '.$inner_glow.';"></div>';
			}
			else {
				$glow = '';
			}

			echo '
				<style>
					.block--'.$block_count.' .bg-photo--'.$block_count.' {
						background-image: url('.$photo_url.');
						background-position: 50% 50%;
					}
					
					@media screen and (min-width:650px){
						.block--'.$block_count.' .bg-photo--'.$block_count.' {
							background-position: '.get_sub_field("bg_x_pos").'% '.get_sub_field("bg_y_pos").'%;
						}
					}
					
				</style>
			';

			echo '<div class="bg-photo--'.$block_count.'">'.$overlay.''.$gradient.''.$glow.'</div>';

		endif; //end $content_type == 'photo';

		echo '</div>'; //current column wrapper

		$current_column++;

	endwhile; //end columns

	echo '</div>'; //.block
?>