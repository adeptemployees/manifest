<?php wp_enqueue_script("match-height", get_template_directory_uri()."/js/plugins/match-height.js", array('jquery'), false, true ); ?>

<div class="block--<?php echo $block_count; ?> sub-section-summaries">
	<div class="container">
		
		<?php the_sub_field('intro_content'); ?>
		
		<?php 
			$column_count = get_sub_field('column_count');
			$card_count = count(get_sub_field('cards'));
 			$odd_card_count = ($card_count % $column_count !== 0 ? 'odd-card-count' : '');
 			
			$text_alignment = get_sub_field('card_text_alignment');
			echo '<div class="card-wrapper columns-'.$column_count.' '.$odd_card_count.'" style="text-align:'.$text_alignment.';">';

			while(has_sub_field('cards')) :
				$card_classes = '';
				//get image
				$img_id = get_sub_field('image');
				if($img_id) {
					$image = wp_get_attachment_image_src($img_id, 'thumb');
					$image_alt = get_post_meta($img_id, '_wp_attachment_image_alt', true);
					$image_position = get_sub_field('image_position');
					$card_classes .= 'featured-image '.$image_position.' ';
				}

				//card description
				$card_description = get_sub_field('card_description');
				$card_description = strip_tags($card_description, '<br><strong><em><span>');

				//check if card will be a link
				$card_link = get_sub_field('card_link');
				if($card_link) {
					$card_classes .= 'has-link ';
				}

		?>
				<div class="card match-height <?php echo $card_classes; ?>" <?php if($card_link) {echo 'onclick="window.location.href=\''.$card_link.'\'"';} ?>>
					<?php if($img_id) {echo '<div class="card-image"><img src="'.$image[0].'" width="'.$image[1].'" height="'.$image[2].'" alt="'.$image_alt.'"></div>';} ?>
					<div class="card-text">
						<p class="card-title" style="color:<?php the_sub_field('title_text_color'); ?>;"><strong><?php the_sub_field('card_title'); ?></strong></p>
						<?php 
							if($card_description) {
								echo '<p class="card-description" style="color:'.get_sub_field('description_text_color').';">'.$card_description.'</p>';
							}
							if($card_link) {
								echo '<p class="card-link" style="color:'.get_sub_field('call_to_action_text_color').';">'.get_sub_field('call_to_action_text').'</p>';
							}
						?>
					</div> <!-- card-text -->
				</div> <!-- card -->

		<?php 
			endwhile;

			echo '</div> <!-- card-wrapper -->';
		?>
	
		<?php the_field('outro_content'); ?>

	</div> <!-- container -->
</div> <!-- block -->