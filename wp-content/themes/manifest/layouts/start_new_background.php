<?php 

	//bg color
	$bg_color = get_sub_field('background_color');
	$bg_color_opacity = get_sub_field('background_color_opacity');
	$bg_color_opacity = (int)$bg_color_opacity / 100;
	$bg_color = hex2rgba($bg_color, $bg_color_opacity);

	//bg photo
	$bg_photo_id = get_sub_field('background_photo');
	if($bg_photo_id) {
		$bg_photo = wp_get_attachment_image_src($bg_photo_id, 'full');
		$bg_photo_url = aq_resize($bg_photo[0], 1500, null, true, true, true);

		echo '
			<style>
				.block--'.$block_count.' .new-bg--image {
					background-image: url('.$bg_photo_url.');
					background-repeat: no-repeat;
					background-position: center center;
					background-size: cover;
					z-index: '.get_sub_field('background_depth').';
				}
				.block--'.$block_count.' .new-bg--image .new-bg--opacity {
					background-color: '.$bg_color.';
				}
				@media screen and (min-width: 1500px) {
					.block--'.$block_count.' .new-bg--image {
						background-size: 100% auto;
					}
				}
			</style>';
	}

	//bg gradient
	if(get_sub_field('background_gradient')){
		$gradient_color = get_sub_field('gradient_color');
		//$gradient_color_transparent = hex2rgba($gradient_color, .001);
		$gradient_dest = get_sub_field('background_color');
		$gradient_opacity = get_sub_field('gradient_opacity');
		$gradient_opacity = (int)$gradient_opacity / 100;
		$gradient_color = hex2rgba($gradient_color, $gradient_opacity);

		$gradient_start_position = get_sub_field('gradient_start_position');
		if($gradient_start_position === 'top') {
			$bg_gradient = 'linear-gradient('.$gradient_color.', '.$gradient_dest.')';
		}
		else { //$gradient_start_position === 'bottom'
			$bg_gradient = 'linear-gradient(bottom, '.$gradient_color.', '.$gradient_dest.')';
		}
		$bg_gradient_styles = 'background: -webkit-'.$bg_gradient.'; background: -o-'.$bg_gradient.'; background: -moz-'.$bg_gradient.'; background: '.$bg_gradient.';';
	}
	else {
		$bg_gradient_styles = '';
	}

	//inner glow
	if(get_sub_field('inner_glow')){
		$glow_color = get_sub_field('glow_color');
		$glow_opacity = get_sub_field('glow_opacity');
		$glow_opacity = (int)$glow_opacity / 100;+
		$glow_color = hex2rgba($glow_color, $glow_opacity);
		$glow_size = get_sub_field('glow_size');

		$inner_glow = 'inset 0 0 '.$glow_size.'px '.$glow_color.';';
		$inner_glow_styles = '-moz-box-shadow: '.$inner_glow.'; -webkit-box-shadow: '.$inner_glow.'; -o-box-shadow: '.$inner_glow.'; box-shadow: '.$inner_glow.';';
	}
	else {
		$inner_glow_styles = '';
	}

	//spacing
	$t_padding = get_sub_field('top_padding');
	$b_padding = get_sub_field('bottom_padding');
	$b_margin = get_sub_field('bottom_margin');

	$background_styles = 'style="background-color:'.$bg_color.'; '.$bg_gradient_styles.' '.$inner_glow_styles.' padding-top:'.$t_padding.'em; padding-bottom:'.$b_padding.'em; margin-bottom:'.$b_margin.'em;"';

?>

<div class="block--<?php echo $block_count; ?> new-background" <?php echo $background_styles; ?>>
	<?php if($bg_photo_id) {echo '<div class="new-bg--image"><div class="new-bg--opacity"></div></div>';} ?>

