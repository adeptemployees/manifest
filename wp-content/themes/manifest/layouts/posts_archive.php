<div class="block--<?php echo $block_count; ?> posts-archive">
<?php
    $post_type = get_sub_field('post_type');
    $layout = get_sub_field('posts_layout');

    switch ($post_type) {
        case 'event':
            include 'parts/type_renderers/events_renderer.php';
            break;

        case 'opportunity':
            switch ($layout) {
                case 'table':
                    include 'parts/type_renderers/opportunities/table_renderer.php';
                    break;
                
                default:
                    echo 'No layout template provided.';
                    break;
            }
            break;
        
        default:
            echo 'No layout template provided.';
            break;
    }
?>
</div>