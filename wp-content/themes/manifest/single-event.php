<?php //single post ?>
<?php get_header(); ?>

<?php
    $start_day = date('j', strtotime(get_field('start_date')));
    $start_month = date('F', strtotime(get_field('start_date')));
    $end_day = date('j', strtotime(get_field('end_date')));
    $end_month = date('F', strtotime(get_field('end_date')));
?>

<div class="container">

    <div class="main-content event-content">
        <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>

            <ul class="event-details">
                <li>
                    <i class="material-icons">event</i>
                    <?php echo date('F j', strtotime(get_field('start_date'))); ?>
                    <?php if(get_field('start_date') != get_field('end_date')): ?>
                        <?php if($start_month == $end_month): ?>
                            - <?php echo $end_day; ?>
                        <?php else: ?>
                            <?php if(get_field('end_date') != ''): ?>
                                - <?php echo $end_month; ?> <?php echo $end_day; ?>
                            <?php endif; ?>
                        <?php endif; ?>  
                    <?php endif; ?>        
                </li>
                <li><i class="material-icons">room</i> <?php echo get_field('location_display_name'); ?></li>
            </ul>
            <h1><?php the_title(); ?></h1>
    
            <?php the_content(); ?>

            <?php if(has_term( 'boot-camps', 'event-types', $post->ID )): ?>
                <section class="apply-form">
                    <header>
                        <h3>Apply Now</h3>
                    </header>
                    <?php gravity_form( 2, false, false, false, array('course_name' => get_the_title(), 'course_id' => $post->ID), false); ?>
                </section>
            <?php endif; ?>
    
        <?php endwhile; endif; ?>
    
    </div> <!-- main-content -->

</div> <!-- container -->

<?php get_footer(); ?>