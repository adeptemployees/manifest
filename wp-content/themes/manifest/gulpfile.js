var gulp       = require('gulp');
var gutil      = require('gulp-util');
var less       = require('gulp-less');
var jshint     = require('gulp-jshint');
var notify     = require('gulp-notify');
var plumber    = require('gulp-plumber');
var stripDebug = require('gulp-strip-debug');
var uglify     = require('gulp-uglify');
var cleanCSS   = require('gulp-clean-css');
var liveReload = require('gulp-livereload');
var prefix     = require('autoprefixer');
var postcss     = require('gulp-postcss');

//ERRORS 
var reportError = function (error) {
    var lineNumber = (error.lineNumber) ? 'LINE ' + error.lineNumber + ' -- ' : '';

    notify({
        title: 'Task Failed [' + error.plugin + ']',
        message: lineNumber + 'See console.',
        sound: 'Sosumi' // See: https://github.com/mikaelbr/node-notifier#all-notification-options-with-their-defaults
    }).write(error);

    gutil.beep();

    // Pretty error reporting
    var report = '';
    var chalk = gutil.colors.white.bgRed;

    report += chalk('TASK:') + ' [' + error.plugin + ']\n';
    report += chalk('PROB:') + ' ' + error.message + '\n';
    if (error.lineNumber) { report += chalk('LINE:') + ' ' + error.lineNumber + '\n'; }
    if (error.fileName)   { report += chalk('FILE:') + ' ' + error.fileName + '\n'; }
    console.error(report);

    // Prevent the 'watch' task from stopping
    this.emit('end');
}

gulp.task('jshint', function() {
    return gulp.src('./js/*.js')
    .pipe(plumber({
        errorHandler: reportError
    }))
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(notify({ message: 'JS Hinting task complete' }));
})

gulp.task('scripts', function() {
    return gulp.src('./js/*.js')
        .pipe(plumber({
            errorHandler: reportError
        }))
        .pipe(stripDebug())
        .pipe(uglify())
        .pipe(gulp.dest('./js/min'))
        .pipe(notify({ message: 'Scripts task complete' }))
})

gulp.task('styles', function() {
    var processors = [
        prefix({browsers: ['last 2 versions']})
    ];
    return gulp.src('./less/master.less')
        .pipe(plumber({
            errorHandler: reportError
        }))
        .pipe(less())
        .pipe(postcss(processors))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./css'))
        .pipe(notify({ message: 'Styles task complete' }))
})


// This handles watching and running tasks as well as telling our LiveReload server to refresh things
gulp.task('watch', function() {
    // Whenever a stylesheet is changed, recompile
    gulp.watch('./less/**/*.less', ['styles']);

    // If user-developed Javascript is modified, re-run our hinter and scripts tasks
    gulp.watch('./js/*.js', ['jshint', 'scripts']);

    // Create a LiveReload server
    var server = liveReload();
});

gulp.task('default', ['jshint', 'scripts', 'styles', 'watch']);
