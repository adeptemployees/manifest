<div class="post-meta">
	<p>
		<span class="no-wrap"><svg style="width:16px;height:16px" viewBox="0 0 24 24">
			<path fill="#999999" d="M17,3H7A2,2 0 0,0 5,5V21L12,18L19,21V5C19,3.89 18.1,3 17,3Z" />
		</svg><?php the_category( ', ' ); ?></span>
		<svg class="circle" style="width:8px;height:6px" viewBox="0 0 24 24">
			<path fill="#999999" d="M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z" />
		</svg>
		<span class="no-wrap"><svg style="width:16px;height:16px" viewBox="0 0 24 24">
			<path fill="#999999" d="M19,19H5V8H19M16,1V3H8V1H6V3H5C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5C21,3.89 20.1,3 19,3H18V1M17,12H12V17H17V12Z" />
		</svg></i><?php the_time('F j, Y'); ?></span>
	</p>
</div>