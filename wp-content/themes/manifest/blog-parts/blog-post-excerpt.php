<div class="blog-post--excerpt">
	<h3><a href="<?php the_permalink(); ?>" class="article-title blue-text"><?php the_title(); ?></a></h3>

	<?php include('post-meta.php'); ?>

	<?php
		//get featured image or first image in post
		if ( has_post_thumbnail($post->ID) ) {	
			$img = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
		} elseif( blog_first_image() ) {
			$img = blog_first_image();
		}
		$img = aq_resize($img, 270, 180, true, true, true);
		if($img && $img !== "") {
			echo '<a href="'.get_permalink().'"><img class="alignright" src="'.$img.'"></a>';
		}
	?>

	<div class="excerpt">
		<?php
			the_excerpt();
		?>
		<a class ="blue-link" href="<?php the_permalink(); ?>">Read Article <svg style="width:18px;height:18px" viewBox="0 0 24 24">
			<path fill="#002269" d="M4,11V13H16L10.5,18.5L11.92,19.92L19.84,12L11.92,4.08L10.5,5.5L16,11H4Z" /></path></svg>
		</a>
		
	</div> <!-- post-excerpt -->

</div> <!-- blog-post -->