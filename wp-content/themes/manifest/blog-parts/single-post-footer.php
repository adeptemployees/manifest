
<?php 
	//related posts
	$orig_post = $post;
	global $post;
	$categories = get_the_category($post->ID);
	if ($categories) :

		echo '<div class="related-posts"><h4 class="text-teal">RECOMMENDED ARTICLES</h4><ul class="group">';
		
		$category_ids = array();
		foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
		$posts[] = $post->ID;
		$args=array(
			'category__in' => $category_ids,
			'post__not_in' => $posts,
			'posts_per_page' => 4,
			'caller_get_posts' =>1 
		);
		$my_query = new WP_Query( $args );
		if( $my_query->have_posts() ) :
			while( $my_query->have_posts() ) :
				$my_query->the_post(); ?>
			<li>
				<a href="<?php the_permalink(); ?>">
					<?php 
						if ( has_post_thumbnail($post->ID) ) {	
							$thumb = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
						} elseif( blog_first_image() ) {
							$thumb = blog_first_image();
						}
						else {
							$thumb = "";
						}
						$thumb = aq_resize($thumb, 400, 270, true, true, true);
						if($thumb) {
							echo '<img src="'.$thumb.'">';
						}
						else {
							echo '<span class="no-image"></span>';
						}
					?>
				</a>
					<?php include('post-meta.php'); ?>
					<a href="<?php the_permalink(); ?>"><span class="title blue-text"><?php the_title(); ?></span></a>
			</li>
		<?php 
			endwhile; //end query
		endif; //end query

		echo '</ul></div>';
		
	endif; //end $categories 
	$post = $orig_post;
	wp_reset_query(); 
	//end related posts
?>

<!-- <div class="post-navigation">
	<?php
		$prev_post = get_adjacent_post( false, null, true );
		$next_post = get_adjacent_post( false, null, false );

		if ( is_a( $prev_post, 'WP_Post' ) ) {
			echo '<a class="prev" href="'.get_permalink( $prev_post->ID ).'">Prev Post<span>'.get_the_title( $prev_post->ID ).'</span></a>';
		}
		if ( is_a( $next_post, 'WP_Post' ) ) {
			echo '<a class="next" href="'.get_permalink( $next_post->ID ).'">Next Post<span>'.get_the_title( $next_post->ID ).'</span></a>';
		}
	?>
</div> --> <!-- post-navigation -->