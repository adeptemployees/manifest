<?php
    $is_boot_camp = '--simple';
    if(has_term( 'boot-camps', 'event-types', $post->ID )) {
        $is_boot_camp = '--bootcamp';
    }

    $start_day = date('j', strtotime(get_field('start_date')));
    $start_month = date('F', strtotime(get_field('start_date')));
    $end_day = date('j', strtotime(get_field('end_date')));
    $end_month = date('F', strtotime(get_field('end_date')));
?>

<div class="event-post--excerpt<?php echo $is_boot_camp; ?>">
    <div class="excerpt-image">
        <?php
            //get featured image or first image in post
            if ( has_post_thumbnail($post->ID) ) {  
                $img = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
            } elseif( blog_first_image() ) {
                $img = blog_first_image();
            }
            //$img = aq_resize($img, 150, 150, true, true, true);
            if($img && $img !== "") {
                echo '<a href="'.get_permalink().'"><img src="'.$img.'"></a>';
            }
        ?>
    </div>

    <div class="excerpt-content">
        <ul class="event-details">
            <li>
                <i class="material-icons">event</i> 
                <?php echo date('F j', strtotime(get_field('start_date'))); ?>
                <?php if(get_field('start_date') != get_field('end_date')): ?>
                    <?php if($start_month == $end_month): ?>
                        - <?php echo $end_day; ?>
                    <?php else: ?>
                        <?php if(get_field('end_date') != ''): ?>
                            - <?php echo $end_month; ?> <?php echo $end_day; ?>
                        <?php endif; ?>
                    <?php endif; ?>  
                <?php endif; ?>        
            </li>
            <li><i class="material-icons">room</i> <?php echo get_field('location_display_name'); ?></li>
        </ul>
        <h3 class="event-post--excerpt-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

        <?php
            the_excerpt();
            if(has_term( 'boot-camps', 'event-types', $post->ID )) {
                echo '<p><a class="excerpt-action" href="'.get_the_permalink().'">Learn More &amp; Apply <i class="material-icons" data-event-type="bootcamp">call_made</i></a></p>';
            } else {
                echo '<p><a class="excerpt-action" href="'.get_the_permalink().'">Learn More <i class="material-icons">call_made</i></a></p>';
            }
        ?>

    </div>

</div> <!-- blog-post -->