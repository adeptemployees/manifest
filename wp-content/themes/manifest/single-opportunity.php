<?php //single post ?>
<?php get_header(); ?>

<div class="container">

    <div class="main-content opp-content">
        <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>

            <h1><?php the_title(); ?></h1>
    
            <?php the_content(); ?>

            <p><strong>Applicants only, no third parties or agencies please.</strong></p>

            <footer>
                <div class="apply-form">
                    <header>
                        <h3>Apply Now</h3>
                    </header>
                    <?php gravity_form( 3, false, false, false, array('opportunity_name' => get_the_title(), 'opportunity_id' => $post->ID), false); ?>
                </div>
                <div class="opp-disclaimer">
                    <?php echo get_field('opportunity_application_disclaimer', 'options'); ?>
                </div>
            </footer>
    
        <?php endwhile; endif; ?>
    
    </div> <!-- main-content -->

</div> <!-- container -->

<?php get_footer(); ?>