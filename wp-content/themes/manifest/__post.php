<?php //single post ?>
<?php get_header(); ?>


<div class="container">

	<div class="main-content <?php echo get_post_format($post->ID); ?>">
		<?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>
		
			<?php get_template_part('blog-parts/single-post-header'); ?>
	
			<div class="blog-post-content">
				<?php the_content(); ?>
	
			</div> <!-- blog-post-content -->
	
			<?php get_template_part('blog-parts/single-post-footer'); ?>
	
		<?php endwhile; endif; ?>
	
	</div> <!-- main-content -->
	
	<?php get_template_part('sidebar'); ?>	

</div> <!-- container -->

<?php get_footer(); ?>