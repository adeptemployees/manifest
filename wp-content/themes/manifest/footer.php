    </div> <!-- content-wrapper -->

    <footer class="site-footer">

        <div class="container">
            
            <nav class="footer-main-nav group">
                <?php
                $args = array(
                    'theme_location' => 'footer_nav',
                    'menu' => 'footer_nav',
                    'container' => '',
                    'menu_class' => 'footer-main',
                    'echo' => true,
                    'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
                    'depth' => 0
                    );
                wp_nav_menu( $args ); 
                ?>
            </nav>
            
            <div class="footer-contact-info" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <?php 
                    if( get_field('official_name', 'options') ) { echo '<span itemprop="name"><strong>'.get_field('official_name', 'options').'</strong></span>'; }
                    if( get_field('address_line_1', 'options') ) { echo '<span itemprop="streetAddress">'.get_field('address_line_1', 'options').' </span>'; }
                    if( get_field('address_line_2', 'options') ) { echo '<span>'.get_field('address_line_2', 'options').' </span>'; }
                    if( get_field('city', 'options') && get_field('state', 'options') && get_field('zip_code', 'options') ) { echo '<span itemprop="addressLocality">'.get_field('city', 'options').'</span>, <span itemprop="addressRegion">'.get_field('state', 'options').'</span> <span itemprop="postalCode">'.get_field('zip_code','options').' </span>'; }
                    if( get_field('phone_number', 'options') ) { echo '<span itemprop="telephone">'.get_field('phone_number', 'options').' </span>'; }
                    if( get_field('fax_number', 'options') ) { echo '<span itemprop="faxNumber">'.get_field('fax_number', 'options').' </span>'; }
                    if( get_field('email_address', 'options') ) { echo '<span itemprop="email"><a href="mailto:'.get_field('email_address', 'options').'">'.get_field('email_address', 'options').'</a> </span>'; }
                ?>

            </div> <!-- contact-info -->

            <nav class="footer-social-nav">
                <ul>
                    <?php 
                        if(get_field('facebook_url', 'options')) { echo '<li><a href="'.get_field('facebook_url','options').'" target="_blank"><i class="fa fa-fw fa-facebook"></i> <span>Facebook</span></a></li>'; }
                        if(get_field('twitter_url', 'options')) { echo '<li><a href="'.get_field('twitter_url','options').'" target="_blank"><i class="fa fa-fw fa-twitter"></i> <span>Twitter</span></a></li>'; }
                        if(get_field('linkedin_url', 'options')) { echo '<li><a href="'.get_field('linkedin_url','options').'" target="_blank"><i class="fa fa-fw fa-linkedin"></i> <span>LinkedIn</span></a></li>'; }
                        if(get_field('googleplus_url', 'options')) { echo '<li><a href="'.get_field('googleplus_url','options').'" target="_blank"><i class="fa fa-fw fa-google-plus"></i> <span>Google+</span></a></li>'; }
                        if(get_field('youtube_url', 'options')) { echo '<li><a href="'.get_field('youtube_url','options').'" target="_blank"><i class="fa fa-fw fa-youtube"></i> <span>YouTube</span></a></li>'; }
                        if(get_field('blog_url', 'options')) { echo '<li><a href="'.get_field('blog_url','options').'"><i class="fa fa-fw fa-wordpress"></i> <span>Blog</span></a></li>'; }
                    ?>
                </ul>
            </nav>
        </div> <!-- container -->
    </footer>

</div> <!-- site-wrapper -->


<?php
    wp_enqueue_script("media-check", get_template_directory_uri()."/js/plugins/media-check.js", array("jquery"), false, true);
    wp_enqueue_script("priority-nav", get_template_directory_uri()."/js/plugins/priority-nav.js", array("jquery"), false, true);
    wp_enqueue_script("sticky-kit", get_template_directory_uri()."/js/plugins/sticky-kit.js", array("jquery"), false, true);
    
    //this script is only needed for IE9 for the priority nav plugin to work.
    wp_register_script("classList", "http://cdnjs.cloudflare.com/ajax/libs/es5-shim/2.0.8/es5-shim.min.js", array("jquery", "priority-nav"), false, true);
    wp_enqueue_script("classList");
    wp_script_add_data("classList", "conditional", "lt IE 9");

    wp_enqueue_script("nav", get_template_directory_uri()."/js/nav.js", array("media-check"), false, true );
?>

<?php wp_footer(); ?>
</body>
</html>