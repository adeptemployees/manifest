<?php

    //show_admin_bar(false);

    //image resizer 
    require_once('lib/aq_resizer.php');

    //add acf layout selector
    require_once('lib/acf_layout.php');
    
    
    function jquery_deregister() {
        //load jquery
        if (!is_admin()) {
            wp_deregister_script('jquery');
            wp_register_script('jquery', ("http".($_SERVER['SERVER_PORT'] == 443 ? "s" : "")."://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"), false, '1.11.3',true);
            wp_enqueue_script('jquery');
        }
    }
    add_action('wp_enqueue_scripts', 'jquery_deregister');

    function add_stylesheets() {
        //load stylesheets
        if (!is_admin()) {
            wp_enqueue_style('fonts-com', '//fast.fonts.net/cssapi/04605692-cbb0-4484-9155-96e3c6c87639.css', false, false, 'all');
            wp_enqueue_style('google-merriweather', '//fonts.googleapis.com/css?family=Merriweather', false, false, 'all');
            wp_enqueue_style('font-awesome','https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css', false, false, 'all');
            wp_enqueue_style('material-ui-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons', false, false, 'all');
            wp_enqueue_style('master', get_template_directory_uri().'/css/master.css', array('fonts-com', 'google-merriweather', 'font-awesome', 'material-ui-icons'), '1.0.0', 'all');
        }
    }
    add_action('wp_enqueue_scripts', 'add_stylesheets');

    function add_analytics() {
        wp_enqueue_script('analytics', get_template_directory_uri().'/js/analytics.js', false, null, false);
    }
    add_action('wp_enqueue_scripts', 'add_analytics');


    // ==========================================================================
    //    WORDPRESS STUFF
    // ==========================================================================

        //enable featured images
        add_theme_support('post-thumbnails');
    
        //enable post formats
        //available post formats: https://codex.wordpress.org/Post_Formats 
        add_theme_support( 'post-formats', array( 'video', 'gallery' ) );
    
        //menus
        function register_nav() {
            register_nav_menu('main_nav',__( 'Main Menu' ));
            register_nav_menu('util_nav',__( 'Utility Menu' ));
            register_nav_menu('footer_nav',__( 'Footer Menu' ));
        } add_action( 'init', 'register_nav' );
    

        //widgets
        function register_widget_area() {
            register_sidebar( array(
                'name'          => 'Blog Widgets',
                'id'            => 'blog-widgets',
                'before_widget' => '<div class="blog-widgets">',
                'after_widget'  => '</div>',
                'before_title'  => '<h4 class="section-title">',
                'after_title'   => '</h4>',
            ));
        } add_action( 'widgets_init', 'register_widget_area' );

        //change wp logo on wp-login.php
        function custom_login_logo() {
            echo '<style type="text/css">
            .login h1 a { background-image: url('.get_bloginfo('template_directory').'/img/mobile_logo.png); background-size: 100%; width: 100%;}
            </style>';
        }
        add_action('login_head', 'custom_login_logo');

        //useful functions
        function get_the_slug() {
            $slug = basename(get_permalink());
            return $slug;
        }

        function get_top_level_parent_slug() {
            global $post;
            $parent = array_reverse(get_post_ancestors($post->ID));
            $first_parent = get_page($parent[0]);
            return $first_parent->post_name;
        }

        function get_the_parent_slug() {
            global $post;
            if($post->post_parent == 0) return '';
            $post_data = get_post($post->post_parent);
            return $post_data->post_name;
        }

        // hide the hellip from blog excerpt
        // function trim_excerpt($text){
        //     return rtrim(str_replace('[&hellip', '', $text), '[...]');
        // }
        // add_filter('get_the_excerpt', 'trim_excerpt');


        //check if in blog pages 
        function is_blog() {
            global  $post;
            $posttype = get_post_type($post );
            return ( ( is_archive() || is_author() || is_category() || is_home() || is_single() || is_tag() ) && ( $posttype == 'post' ) ) ? true : false;
        }
        
        //add class to blog nav when in blog pages
        function blog_nav_class( $classes, $item ) {
            if ( is_blog() && $item->title == 'Blog' ) {
                $classes[] = 'current_page_item';
            }
            return $classes;
        }
        add_filter( 'nav_menu_css_class', 'blog_nav_class', 10, 2 );

        //custom blog excerpt
        // function new_excerpt_more($more) {
        //     global $post;
        //     return '... <br><a class="btn secondary-btn read-more" href="'. get_permalink($post->ID) . '">Read More</a>';
        // }
        // add_filter('excerpt_more', 'new_excerpt_more');

        //get first image from blog post 
        function blog_first_image() {
            global $post, $posts;
            $first_img = '';
            ob_start();
            ob_end_clean();
            $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
            $first_img = $matches[1][0];

            if(empty($first_img)) {
                return false;
            }
            return $first_img;
        }

        //only search posts when searching
        function searchfilter($query) {
            if ($query->is_search && !is_admin() ) {
                $query->set('post_type',array('post'));
            }
            return $query;
        }
        add_filter('pre_get_posts','searchfilter');

        //wrap youtube embed code in a class to make it responsive.. otherwise just return the typical embed code
        function custom_oembed_filter($html, $url, $attr, $post_ID) {
            if(strpos($url,'youtube') !== false || strpos($url,'youtu.be') !== false || strpos($url,'vimeo') !== false) {
                return '<div class="flexible-video">'.$html.'</div>';
            }
            else {
                return $html;
            }
        }
        add_filter( 'embed_oembed_html', 'custom_oembed_filter', 10, 4 ) ;
        
        //remove link to image 
        function wpb_imagelink_setup() {
            $image_set = get_option( 'image_default_link_type' );
            
            if ($image_set !== 'none') {
                update_option('image_default_link_type', 'none');
            }
        }
        add_action('admin_init', 'wpb_imagelink_setup', 10);
        

    // ==========================================================================
    //    ACF STUFF
    // ==========================================================================
    
        //add site options page
        if( function_exists('acf_add_options_page') ) {
            acf_add_options_page(get_bloginfo('name'). ' Globals');
        }
        
        //pre-populate color picker with brand colors
        function load_javascript_on_admin_edit_post_page() {
            global $parent_file;
            // If we're on the edit post page.
            if (
              strpos($parent_file, 'post-new.php') !== false ||
              strpos($parent_file, 'edit.php') !== false ||
              strpos($parent_file, 'post.php') !== false
            ) {
                echo "
                  <script>
                  jQuery(document).ready(function(){
                    if(jQuery('.acf-color_picker').length) {
                        jQuery('.acf-color_picker').iris({
                          palettes: ['#ffffff', '#dddddd', '#555555', '#333333'],
                          change: function(event, ui){
                            jQuery(this).parents('.wp-picker-container').find('.wp-color-result').css('background-color', ui.color.toString());
                          }
                        });
                    }
                  });
                  </script>
                ";
            }
        }
        add_action('in_admin_footer', 'load_javascript_on_admin_edit_post_page');

        //add custom css for acf fields in admin
        function custom_acf_admin_styles() {
            wp_register_style( 'custom-acf-admin-css', get_stylesheet_directory_uri() . '/admin/custom-acf-admin-styles.css', false, '1.0.0' );
            wp_enqueue_style( 'custom-acf-admin-css' );
        }
        add_action( 'acf/input/admin_enqueue_scripts', 'custom_acf_admin_styles' );

    // ==========================================================================
    //    CUSTOM TOOLBAR STUFF
    // ==========================================================================

        //add options to toolbars
        function custom_toolbar( $toolbars ) {

            //add stylselect option to toolbars
            array_unshift( $toolbars['Basic' ][1] , 'styleselect' );
            array_unshift( $toolbars['Full' ][1] , 'styleselect' );

            return $toolbars;
        }
        add_filter( 'acf/fields/wysiwyg/toolbars' , 'custom_toolbar'  );

        function wpb_mce_buttons_2($buttons) {
            array_unshift($buttons, 'styleselect');
            return $buttons;
        }
        add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

        //add custom css styles for wysiwyg editor
        function my_theme_add_editor_styles() {
            add_editor_style( 'admin/custom-editor-styles.css' );
        }
        add_action( 'admin_init', 'my_theme_add_editor_styles' );

        //create custom options in styleselect dropdown
        function custom_format_styles($config) {
            $temp_array = array(
                array(
                    'title' => 'Modifiers',
                    'items' => array(
                        array( 
                            'title' => 'Pre-Headline',
                            'inline' => 'small',
                            'classes' => 'pre-headline'
                        ),
                        array( 
                            'title' => 'All Caps',
                            'inline' => 'span',
                            'classes' => 'all-caps'
                        ),
                        array( 
                            'title' => 'Extra Letter Spacing',
                            'inline' => 'span',
                            'classes' => 'extra-letter-spacing'
                        ),
                        array( 
                            'title' => 'Centered Block',
                            'classes' => 'center-block',
                            'selector' => '*'
                        ),
                        array( 
                            'title' => 'Centered Axis List',
                            'selector' => 'ul',
                            'classes' => 'center-axis-list'
                        ),
                        array( 
                            'title' => 'Rounded Image Corners',
                            'selector' => 'img',
                            'classes' => 'rounded-corners'
                        ),
                        array( 
                            'title' => 'Circle Image',
                            'selector' => 'img',
                            'classes' => 'circle-image'
                        ),
                        array( 
                            'title' => 'Grid List',
                            'selector' => 'ul',
                            'classes' => 'grid-list'
                        ),
                        array( 
                            'title' => 'Plain Text Link',
                            'selector' => 'a',
                            'classes' => 'plain-text'
                        ),
                        array( 
                            'title' => 'Plain Text Link with Arrow',
                            'selector' => 'a',
                            'classes' => 'plain-text--arr'
                        ),
                        array( 
                            'title' => 'Serif Font',
                            'inline' => 'span',
                            'classes' => 'serif-font'
                        ),
                        array( 
                            'title' => 'Thin Header',
                            'inline' => 'span',
                            'classes' => 'thin-header'
                        ),
                        array( 
                            'title' => 'Sub-Headline',
                            'inline' => 'span',
                            'classes' => 'sub-headline'
                        ),
                    )
                ),
                array(
                    'title' => 'Headers',
                    'items' => array(
                        array(
                            'title' => 'Heading 1',
                            'format' => 'h1',
                            'block' => 'h1'
                        ),
                        array(
                            'title' => 'Heading 2',
                            'format' => 'h2',
                            'block' => 'h2'
                        ),
                        array(
                            'title' => 'Heading 3',
                            'format' => 'h3',
                            'block' => 'h3'
                        ),
                        array(
                            'title' => 'Heading 4',
                            'format' => 'h4',
                            'block' => 'h4'
                        ),
                        array(
                            'title' => 'Heading 5',
                            'format' => 'h5',
                            'block' => 'h5'
                        ),
                        array(
                            'title' => 'Heading 6',
                            'format' => 'h6',
                            'block' => 'h6'
                        ),
                    )
                ),
                array(
                    'title' => 'Colors',
                    'items' => array(
                        array(
                            'title' => 'Black',
                            'inline' => 'span',
                            'styles' => array(
                                'color' => '#333333'
                            )
                        ),
                        array(
                            'title' => 'Dark Gray',
                            'inline' => 'span',
                            'styles' => array(
                                'color' => '#555555'
                            )
                        ),
                        array(
                            'title' => 'Gray',
                            'inline' => 'span',
                            'styles' => array(
                                'color' => '#666666'
                            )
                        ),
                        array(
                            'title' => 'Light Gray',
                            'inline' => 'span',
                            'styles' => array(
                                'color' => '#999999'
                            )
                        ),
                        array(
                            'title' => 'White',
                            'inline' => 'span',
                            'classes' => 'white-text',
                            'styles' => array(
                                'color' => '#ffffff',
                            )
                        ),
                        array(
                            'title' => 'Blue',
                            'inline' => 'span',
                            'classes' => 'blue-text',
                            'styles' => array(
                                'color' => '#002269',
                            )
                        ),
                        array(
                            'title' => 'Light-Blue',
                            'inline' => 'span',
                            'classes' => 'light-blue-text',
                            'styles' => array(
                                'color' => '#009bdf',
                            )
                        ),
                        array(
                            'title' => 'Teal',
                            'inline' => 'span',
                            'classes' => 'teal-text',
                            'styles' => array(
                                'color' => '#00c6c6',
                            )
                        ),
                        array(
                            'title' => 'Dark Teal',
                            'inline' => 'span',
                            'classes' => 'dark-teal-text',
                            'styles' => array(
                                'color' => '#009c9f',
                            )
                        ),
                    )
                ),
                array(
                    'title' => 'Buttons',
                    'items' => array(
                        array(
                            'title' => 'Blue Primary Button',
                            'inline' => 'a',
                            'classes' => 'btn--blue_primary'
                        ),
                        array(
                            'title' => 'Blue Text Button',
                            'inline' => 'a',
                            'classes' => 'btn--blue_text'
                        ),
                        array(
                            'title' => 'Teal Primary Button',
                            'inline' => 'a',
                            'classes' => 'btn--teal_primary'
                        ),
                        array(
                            'title' => 'Teal Text Button',
                            'inline' => 'a',
                            'classes' => 'btn--teal_text'
                        ),
                        array(
                            'title' => 'Dark Teal Primary Button',
                            'inline' => 'a',
                            'classes' => 'btn--dark-teal_primary'
                        ),
                        array(
                            'title' => 'Dark Teal Text Button',
                            'inline' => 'a',
                            'classes' => 'btn--dark-teal_text'
                        ),
                        array(
                            'title' => 'White Primary Button',
                            'inline' => 'a',
                            'classes' => 'btn--white_primary'
                        ),
                        array(
                            'title' => 'White Text Button',
                            'inline' => 'a',
                            'classes' => 'btn--white_text'
                        ),
                        array(
                            'title' => 'Gray Primary Button',
                            'inline' => 'a',
                            'classes' => 'btn--gray_primary'
                        ),
                        array(
                            'title' => 'Gray Text Button',
                            'inline' => 'a',
                            'classes' => 'btn--gray_text'
                        ),
                    )
                ),
                array(
                    'title' => 'Font Controls',
                    'items' => array(
                        array(
                            'title' => 'Base + 1',
                            'inline' => 'span',
                            'classes' => 'epsilon'
                        ),
                        array(
                            'title' => 'Base + 2',
                            'inline' => 'span',
                            'classes' => 'delta'
                        ),
                        array(
                            'title' => 'Base + 3',
                            'inline' => 'span',
                            'classes' => 'gamma'
                        ),
                        array(
                            'title' => 'Base + 4',
                            'inline' => 'span',
                            'classes' => 'beta'
                        ),
                        array(
                            'title' => 'Base + 5',
                            'inline' => 'span',
                            'classes' => 'alpha'
                        ),
                        array(
                            'title' => 'Base + 6',
                            'inline' => 'span',
                            'classes' => 'mega'
                        ),
                        array(
                            'title' => 'Base + 7',
                            'inline' => 'span',
                            'classes' => 'giga'
                        ),
                        array(
                            'title' => 'Base + 8',
                            'inline' => 'span',
                            'classes' => 'tera'
                        ),
                    ),
                ),
            );
            $config['style_formats'] = json_encode( $temp_array );
            return $config;
        }
        add_filter('tiny_mce_before_init', 'custom_format_styles');



    // ==========================================================================
    //    GRAVITY FORMS
    // ==========================================================================

        function gform_script_inject() {
            wp_enqueue_script( 'gforms-uploader', get_template_directory_uri().'/js/forms.js', array('jquery'), null, true);
        }
        add_action( 'gform_enqueue_scripts', 'gform_script_inject', 10, 2 );

        add_filter( 'gform_ajax_spinner_url', 'tgm_io_custom_gforms_spinner' );
        /**
         * Changes the default Gravity Forms AJAX spinner.
         *
         * @since 1.0.0
         *
         * @param string $src  The default spinner URL.
         * @return string $src The new spinner URL.
         */
        function tgm_io_custom_gforms_spinner( $src ) {

            return get_stylesheet_directory_uri() . '/img/ajax-loader.gif';
            
        }
    
        //gravity forms force load jquery in footer 
        function init_scripts() {
            return true;
        }
        add_filter("gform_init_scripts_footer", "init_scripts");


        //add class name to gravity forms submit buttons
        function form_submit_button( $button, $form ) {
            return '<input type="submit" id="gform_submit_button_'.$form["id"].'" class="btn--teal_primary" value="'.$form["button"]["text"].'">';
        }
        add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
        

        //show option to hide labels in gravity forms admin side
        add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

        function track_ga_event( $entry, $form ){
            wp_register_script( 'analytics_events', get_template_directory_uri().'/js/events.js' );
            switch ($form['id']) {
                case 1:
                    $tracking_name = 'Contact Us';
                    break;

                case 2:
                    $tracking_name = 'Bootcamp Application';
                    break;

                case 3:
                    $tracking_name = 'Job Application';
                    break;
                
                default:
                    $tracking_name = 'Misc. Form Submission';
                    break;
            }
            $submission_details = array(
                'form_name' => $tracking_name
            );
            wp_localize_script( 'analytics_events', 'submission', $submission_details );
            wp_enqueue_script( 'analytics_events', false, array('analytics'), null, true );
        }
        add_action( 'gform_after_submission', 'track_ga_event', 10, 2 );

        //remove stylesheet
        function remove_gravityforms_style() {
            wp_dequeue_style('gforms_css');
        }
        add_action('wp_print_styles', 'remove_gravityforms_style');
        

    // ==========================================================================
    //    CONVERT HEX TO RGB(A) STRING
    // ==========================================================================

        function hex2rgba($color, $opacity = false) {
            $default = 'rgb(0,0,0)';

            //Return default if no color provided
            if(empty($color))
                return $default; 

            //Sanitize $color if "#" is provided 
            if ($color[0] == '#' ) {
                $color = substr( $color, 1 );
            }

            //Check if color has 6 or 3 characters and get values
            if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
            } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
            } else {
                return $default;
            }

            //Convert hexadec to rgb
            $rgb =  array_map('hexdec', $hex);

            //Check if opacity is set(rgba or rgb)
            if($opacity){
                if(abs($opacity) > 1)
                    $opacity = 1.0;
                
                $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
            } else {
                $output = 'rgb('.implode(",",$rgb).')';
            }

            //Return rgb(a) color string
            return $output;
        }

    // ==========================================================================
    //    BREADCRUMBS FUNCTION
    // ==========================================================================
    
        function get_breadcrumbs() {
            global $post;
            echo '<ul id="breadcrumbs">';
            echo '<li><a href="';
            echo get_option('home');
            echo '">';
            echo 'Home';
            echo '</a></li><li class="separator"> &raquo; </li>';
            if (is_category() || is_single()) {
                echo '<li><a href="'.get_permalink(13).'">Blog</a></li><li class="separator"> &raquo; </li>';
                echo '<li>';
                the_category(' </li><li class="separator"> &raquo; </li><li> ');
                if (is_single()) {
                    echo '</li><li class="separator"> &raquo; </li><li>';
                    the_title();
                    echo '</li>';
                }
            }
            elseif(is_month()) {
                echo '<li><a href="'.get_permalink('13').'">Blog</a></li><li class="separator"> &raquo; </li>';
                echo '<li>'.get_the_date( _x( 'F Y', 'monthly archives date format', 'oia' ) ).'</li>';
            }
            elseif(is_home()) {
                echo '<li>Blog</li>';
            }
            elseif(is_search()) {
                echo '<li><a href="'.get_permalink(13).'">Blog</a></li>';
            }
            elseif (is_page()) {
                if($post->post_parent){
                    $anc = get_post_ancestors( $post->ID );
                    $title = get_the_title();
                    foreach ( $anc as $ancestor ) {
                        $output = '<li><a href="'.get_permalink($ancestor).'" title="'.get_the_title($ancestor).'">'.get_the_title($ancestor).'</a></li> <li class="separator"> &raquo; </li>';
                    }
                    echo $output;
                    echo '<li><strong title="'.$title.'"> '.$title.'</strong></li>';
                } else {
                    echo '<li><strong> '.get_the_title().'</strong></li>';
                }
            }
            echo '</ul>';
        }
    
    // ==========================================================================
    //    SHORTCODE
    // ==========================================================================
        //shortcode to list all pages optional attribute to exclude page ids example: [sitemap exclude="3,10,12"] 
        function sitemap_shortcode( $atts ) {
            $all_pages = wp_list_pages('title_li=&echo=0&exclude='.$atts['exclude']);
            return '<ul class="sitemap">'.$all_pages.'</ul>';
        }
        add_shortcode( 'sitemap', 'sitemap_shortcode' );

    // ==========================================================================
    //    REGISTER SPECIAL SCRIPTS
    // ==========================================================================
        //load slick.js for sliders
        wp_register_script( 'slick', get_stylesheet_directory_uri() . '/js/plugins/slick.js', 'jquery', '', true );
        wp_register_script( 'sliders', get_stylesheet_directory_uri() . '/js/sliders.js', array('jquery', 'slick'), '', true );
        wp_enqueue_script( 'click_events', get_stylesheet_directory_uri().'/js/click_events.js', array('analytics', 'jquery') );


    // ==========================================================================
    //    ADDING DESCRIPTIONS TO LINKS IN NAV
    // ==========================================================================
    class Menu_With_Description extends Walker_Nav_Menu {
        function start_el(&$output, $item, $depth, $args) {
            global $wp_query;
            $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
            
            $class_names = $value = '';

            $classes = empty( $item->classes ) ? array() : (array) $item->classes;

            $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
            $class_names = ' class="' . esc_attr( $class_names ) . '"';

            $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

            $attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
            $attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
            $attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
            $attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';

            $item_output = $args->before;
            $item_output .= '<a'. $attributes .'>';
            $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
            if(!empty($item->description)) {
                $item_output .= '<br /><span class="sub">' . $item->description . '</span>';
            }
            $item_output .= '</a>';
            $item_output .= $args->after;

            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
        }
    }

    // ==========================================================================
    //    CUSTOM DISPLAY FOR POST OBJECTS (ACF)
    // ==========================================================================
    function post_object_results_rewrite( $title, $post, $field, $post_id ) {

        // add post type to each result
        $title .= ' (Posted: ' . date("n/j/Y g:ia", strtotime($post->post_date)) .  ' - ID: ' . $post_id . ')';
        return $title;

    }
    add_filter('acf/fields/post_object/result', 'post_object_results_rewrite', 10, 4);

    function post_object_query( $args, $field, $post_id ) {

        $meta_query = array(
            array(
                'key' => 'can_include',
                'value' => true,
                'compare' => '='
            )
        );
        $args['meta_query'] = $meta_query;
        return $args;
        
    }
    // filter for every field
    add_filter('acf/fields/post_object/query', 'post_object_query', 10, 3);


    /**
     *  Function: pd_get_excerpt_from_content()
     *  Description: Strip tags and return substring of content
     *  Parameters: $postcontent: the_content(), $length: INT how many chars to return
     *  Return: Substring of the_content();
     */
    function pd_get_excerpt_from_content($postcontent, $length = 100){

        $has_ellip = false;
        $this_excerpt = strip_shortcodes( $postcontent );
        $this_excerpt = strip_tags($this_excerpt);

        $raw_length = strlen($this_excerpt);
        if($raw_length > $length) {
            $has_ellip = true;
        }

        $this_excerpt = substr($this_excerpt, 0, $length);

        if($has_ellip) {
            $this_excerpt .= '...';
        }

        return $this_excerpt;
    }


?>