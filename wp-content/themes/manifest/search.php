<?php 
/*
 * The template for search results
 */
?>

<?php get_header(); ?>


<div class="container">
	<div class="main-content blog-posts-archive">
		
		<?php
			if ( have_posts() ) : while ( have_posts() ) : the_post(); 

				get_template_part('blog-parts/blog-post-excerpt');

			endwhile; endif; 
			
			get_template_part('blog-parts/blog-pagination'); 
		?>

	</div> <!-- main-content -->
	
	<?php get_template_part('sidebar'); ?>

</div> <!-- container -->


<?php get_footer(); ?>