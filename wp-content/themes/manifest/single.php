<?php //single post ?>
<?php get_header(); ?>

<div class="blog is-single-post">

	<div class="container">

		<div class="main-content <?php echo get_post_format($post->ID); ?>">
			<?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>
			
				<?php get_template_part('blog-parts/single-post-header'); ?>

				<h4 class="light-blue-text"> <?php echo get_field('post_intro'); ?> </h4>
		
				<div class="blog-post-content">
					<?php if(has_post_thumbnail()) : ?>
						<div class="feature-img">
							<?php the_post_thumbnail('large'); ?>
						</div>
					<?php endif; ?>
					<div class="content-container">
						<?php the_content(); ?>
					</div>
					<div class="share-this">
						<span>Share<br>This</span>
		                <ul class="">
							<li><a href="https://twitter.com/home?status=<?php the_permalink()?>" target="_blank"><i class="fa fa-fw fa-twitter"></i> <span>Twitter</span></a></li>
							<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink()?>" target="_blank"><i class="fa fa-fw fa-facebook"></i> <span>Facebook</span></a></li>
							<li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink()?>" target="_blank"><i class="fa fa-fw fa-linkedin fa-2x"></i> <span>Linked In</span></a></li>
		                </ul>
	            	</div>
				</div> <!-- blog-post-content -->
		
				<?php get_template_part('blog-parts/single-post-footer'); ?>
		
			<?php endwhile; endif; ?>
		
		</div> <!-- main-content -->
		
		<?php get_template_part('sidebar'); ?>	

	</div> <!-- container -->

</div> <!-- single-post -->

<?php get_footer(); ?>